
var addAsync=(a,b)=>{
    return new Promise((resolve,reject)=>{

        setTimeout(()=>{
            if(typeof a ==='number' && typeof b=== 'number'){
                resolve(a+b);
        }else{
            reject('Arguments must be numbers!');
        }
        },1500)
       
    })
}

 addAsync(10,'20')
    .then((result)=>{
        console.log(`In first then. result:${result}`);
        return addAsync(10,result);},
        (error)=>{console.log(`In first error handler,${error}`)})
    .then((result)=>{
        console.log(`in second then, result is: ${result}`) },
        (error)=>{console.log(`In second error handler,${error}`)});



//Handle error in better way!

addAsync(10,'20')
    .then((result)=>{
        console.log(`In first then. result:${result}`);
        return addAsync(10,result);})
    .then((result)=>{
        console.log(`in second then, result is: ${result}`) })
        .catch((error)=>{
            console.log(error)
        });