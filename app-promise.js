
const axios = require('axios');
const yargs = require('yargs');


//command line help command setup and read arguments
const args=yargs
            .options({
                  a: {
                        demand:true,
                        alias:'address',
                        describe:'To fetch weather.',
                        string:true
                    }
            }).help()
              .argv; 


  var encodedURI= encodeURIComponent(args.address)+'&key=AIzaSyA5shiVdUFG52Rnolh93tdkTpkb7tFkesM'
  var geoCodeUrl=`https://maps.googleapis.com/maps/api/geocode/json?address=${encodedURI}`;
 
  axios.get(geoCodeUrl)
       .then((response)=>{
        if(response.data.status==="ZERO_RESULTS"){
            throw new Error('Unable to find that address!');
        }else{

            var lat=response.data.results[0].geometry.location.lat;
            var lng=response.data.results[0].geometry.location.lng;
            var weatherUrl=`https://api.darksky.net/forecast/2b1af8d7797b25b3c272321c0c5af227/${lat},${lng}`;

            return axios.get(weatherUrl);
        }
        }).then((response)=>{
            if(response.data.statusCode===400){
                throw new Error('Unable to fetch weather!');
            }else{
                var temperature=  response.data.currently.temperature;
                var apparentTemperature=response.data.currently.apparentTemperature;
                console.log(`It's currently ${temperature} but it feels like ${apparentTemperature}`);
            }
        })
        .catch((e)=>{
            if(e.code==='ENOTFOUND'){'Unable to connect to server!'}
            else{
                console.log(e.message);
            }
        })