
const request=require('request');


var getWeather=(lat,lng,callback)=>{
   
    request({
        url:`https://api.darksky.net/forecast/2b1af8d7797b25b3c272321c0c5af227/${lat},${lng}`,
        json:true
    },(error,response,body)=>{
        if(error){
            callback('Unable to connect to weather forcast server!'); 
           }
        else if(response.statusCode===400){
            callback('Unable to fetch weather!'); 
        }else if(!error && response.statusCode===200){
           callback(undefined,{'temperature': body.currently.temperature,'apparentTemperature':body.currently.apparentTemperature});
        }else{
            callback('Unable to fetch weather!'); 
        }
    });
};

module.exports={
    getWeather
}