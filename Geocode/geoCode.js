
const request=require('request');

var geoCodeAddress=(address,callback)=>{
    //CDC Towers, Industrial Development Area, Nacharam, Secunderabad, Telangana
    var encodedURI= encodeURIComponent(address)+'&key=AIzaSyA5shiVdUFG52Rnolh93tdkTpkb7tFkesM'

    request({
           url:`https://maps.googleapis.com/maps/api/geocode/json?address=${encodedURI}`,
           json:true
       },(error,response,body)=>{   

           if(error){
            callback('Unable to connect to server!'); 
           }

           if(body.status==="ZERO_RESULTS"){
            callback('Unable to find that address!')
           }
           else if(body.status==="OK"){
                   if(body.results[0]){
                       callback(undefined, {
                        address:body.results[0].formatted_address,
                        lat:body.results[0].geometry.location.lat  ,
                        lng:body.results[0].geometry.location.lng                       
                       })                  
                   }else{
                    callback("OVER_QUERY_LIMIT");
                   }
               }
       })
}

var geoCodeAddressAsync=(address)=>{
     //CDC Towers, Industrial Development Area, Nacharam, Secunderabad, Telangana
     var encodedURI= encodeURIComponent(address)+'&key=AIzaSyA5shiVdUFG52Rnolh93tdkTpkb7tFkesM'

        return new Promise((resolve,reject)=>{
           
            request({
                url:`https://maps.googleapis.com/maps/api/geocode/json?address=${encodedURI}`,
                json:true
            },(error,response,body)=>{   
     
                if(error){
                 reject('Unable to connect to server!'); 
                }
     
                if(body.status==="ZERO_RESULTS"){
                 reject('Unable to find that address!')
                }
                else if(body.status==="OK"){
                        if(body.results[0]){
                            resolve({
                             address:body.results[0].formatted_address,
                             lat:body.results[0].geometry.location.lat  ,
                             lng:body.results[0].geometry.location.lng                       
                            })                  
                        }else{
                            reject("OVER_QUERY_LIMIT");
                        }
                    }
            })

        })    
}

module.exports={
    geoCodeAddress,
    geoCodeAddressAsync
}