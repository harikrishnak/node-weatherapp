
const request=require('request');
const yargs = require('yargs');
const geoCode= require('./Geocode/geoCode')
const weather = require('./weatherForecast/weather')

//command line help command setup and read arguments
const args=yargs
            .options({
                  a: {
                        demand:true,
                        alias:'address',
                        describe:'To fetch weather.',
                        string:true
                    }
            }).help()
              .argv;

 if(args.address){   
   
    geoCode.geoCodeAddress(args.address,(errorMessage,results)=>{

        if(errorMessage){
            console.log(errorMessage);
        }
        else{
            if(results && results.lat && results.lng ){
                weather.getWeather(results.lat,results.lng,(errorMessage,weatherResult)=>{
                    if(errorMessage){
                                    console.log(errorMessage);
                                }
                                else{
                                    console.log(`It's currently ${weatherResult.temperature} but it feels like ${weatherResult.apparentTemperature}`);
                                }
                })
            }
        }
     });   
}



